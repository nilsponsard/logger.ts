import * as colors from 'https://deno.land/std@0.90.0/fmt/colors.ts';

export enum levels {
    debug, info, warning, error
}

export let minLevel = levels.debug;

export function log(level: levels, message: string, module = "") {
    if (level >= minLevel) {

        const date = new Date();
        let colorfn = colors.gray;
        let levelStr = '';
        switch (level) {
            case levels.warning:
                colorfn = colors.yellow;
                levelStr = "WARNING";
                break;
            case levels.error:
                colorfn = colors.red;
                levelStr = 'ERROR';
                break;
            case levels.info:
                colorfn = colors.green;
                levelStr = 'info';
                break;
            case levels.debug:
                colorfn = colors.white;
                levelStr = 'debug';
                break;
        }

        console.log(
            colorfn(`[${date.toLocaleString(undefined, { hour: 'numeric', minute: 'numeric' })}](${levelStr}) ${module}: ${message}`));
    }
}
export function debug(message: string, module = "") {
    log(levels.debug, message, module);
}
export function info(message: string, module = "") {
    log(levels.info, message, module);
}
export function warning(message: string, module = "") {
    log(levels.warning, message, module);
}
export function error(message: string, module = "") {
    log(levels.error, message, module);
}
